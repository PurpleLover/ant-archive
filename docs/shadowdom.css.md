# Shadow DOM

DOM generate inside shadow DOM has scoped CSS, which is isolated from global CSS. However, width/height or lineup of container of shadow-dom will be aligned by global CSS (this is what I personally experience, need more time to investigate).

In my experience, only styles should be included in shadow-dom, while event (for emit) should be handle outside of it -> to be reusable.

# Refs

- [MDN shadow DOM](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_shadow_DOM)
- [Brief](https://gist.github.com/praveenpuglia/0832da687ed5a5d7a0907046c9ef1813)
- [In depth](https://github.com/praveenpuglia/shadow-dom-in-depth)
- [Events](https://javascript.info/shadow-dom-events)
- [Events with demo](https://pm.dartus.fr/blog/a-complete-guide-on-shadow-dom-and-event-propagation/)