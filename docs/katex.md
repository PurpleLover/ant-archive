# Upright Greek

From latex, there could be some chracter such as

- `\Updelta`
- `\updelta`

which are the upright version of Greek character `delta`. This is not supported in KaTex yet. So to ensure it works, you can follow [the alternative way](https://github.com/KaTeX/KaTeX/issues/564) or change them to `\mathit{\Delta}` or `\mathit{\delta}` if you have a server to convert tex file first.

