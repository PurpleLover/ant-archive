# Install JDK for Ubuntu 16.04 LTS

In case you need to install JDK to Ubuntu 16.04, which is not supported by traditional way.

```sh
# install from link
wget "link-to-install-jdk"
# create jvm folder
sudo mkdir /path/to
cd /path/to
sudo tar -xvzf "path/to/jdk.tar.gz"
sudo nano /etc/environment
# environment content
JAVA_HOME="/path/to/jdk-16.0.2"
PATH="...:/path/to/jdk-16.0.2"
# activate
sudo update-alternatives --install "/usr/bin/java" "java" "/path/to/jdk-16.0.2/bin/java" 0
sudo update-alternatives --install "/usr/bin/javac" "javac" "/path/to/jdk-16.0.2/bin/javac" 0
sudo update-alternatives --set java /path/to/jdk-16.0.2/bin/java
sudo update-alternatives --set javac /path/to/jdk-16.0.2/bin/javac
# verify
update-alternatives --list java
update-alternatives --list javac
java -version
```

> JDK16.02 for linux is already in this repository

# Ref

- [Install JDK 16 on Ubuntu 16.04 LTS](https://www.javahelps.com/2021/03/install-oracle-jdk-16-on-linux.html)