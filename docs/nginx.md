# Checking error in nGinx

Base on different version of nGinx, the name of error log file will be different. However, the log directory for nginx will be the same.

```sh
cd /var/log/nginx/
cat error.log
```

# 401 response

Sometimes your response size is larger than 16K, consider change the buffer_size to larger.

```sh
client_body_buffer_size 2M;
```

# Request Timeout

```sh
# put in http or server or location
proxy_read_timeout 300;
proxy_connect_timeout 300;
proxy_send_timeout 300;
```

## Ref

- [timeout nginx](https://ubiq.co/tech-blog/increase-request-timeout-nginx/)